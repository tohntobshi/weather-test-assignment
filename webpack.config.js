require('dotenv').config()
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const webpack = require('webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = (env) => {
  if (env.mode === 'development') {
    return {
      entry: './src/index.tsx',
      mode: 'development',
      output: {
        filename: '[name].[hash].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
      },
      devtool: 'inline-source-map',
      module: {
        rules: [
          {
            test: /\.tsx?$/,
            use: 'awesome-typescript-loader'
          },
          {
            test: /\.scss$/,
            use: [
              'style-loader',
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  importLoaders: 1,
                  localIdentName: '[folder]__[local]__[hash:base64:5]'
                }
              },
              'sass-loader'
            ]
          },
          {
            test: /\.css$/,
            use: [
              'style-loader',
              'css-loader'
            ]
          },
          {
            test: /\.(png|svg|jpg|gif)$/,
            use: [
              'file-loader'
            ]
          },
          {
            test: /\.(woff|woff2|eot|ttf|otf)$/,
            use: [
              'file-loader'
            ]
          }
        ]
      },
      resolve: {
        extensions: ['*', '.ts', '.tsx', '.js', '.jsx'],
        alias: {
          ':scenes': path.resolve(__dirname, 'src/scenes/'),
          ':components': path.resolve(__dirname, 'src/components/'),
          ':images': path.resolve(__dirname, 'src/assets/images/'),
          ':types': path.resolve(__dirname, 'src/types'),
          ':constants': path.resolve(__dirname, 'src/constants'),
          ':utils': path.resolve(__dirname, 'src/utils'),
          ':api': path.resolve(__dirname, 'src/api'),
          ':actions': path.resolve(__dirname, 'src/redux/actions/'),
          ':sharedStyles': path.resolve(__dirname, 'src/styles/index.scss')
        }
      },
      devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3000,
        watchContentBase: true,
        hot: true,
        historyApiFallback: true
      },
      plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
          template: path.resolve(__dirname, 'index.html')
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
          ENV_BACKEND_ADDRESS: JSON.stringify(process.env.BACKEND_ADDRESS || '')
        })
      ]
    }
  } else {
    return {
      entry: ['babel-polyfill', './src/index.tsx'],
      mode: 'production',
      output: {
        filename: '[name].[hash].js',
        chunkFilename: '[name].[hash].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
      },
      module: {
        rules: [
          {
            test: /\.tsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
          },
          {
            test: /\.scss$/,
            use: [
              MiniCssExtractPlugin.loader,
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  importLoaders: 2,
                  localIdentName: '[folder]__[local]__[hash:base64:5]'
                }
              },
              {
                loader: 'sass-loader'
              }
            ]
          },
          {
            test: /\.css$/,
            use: [
              MiniCssExtractPlugin.loader,
              'css-loader'
            ]
          },
          {
            test: /\.(png|svg|jpg|gif)$/,
            use: [
              'file-loader'
            ]
          },
          {
            test: /\.(woff|woff2|eot|ttf|otf)$/,
            use: [
              'file-loader'
            ]
          }
        ]
      },
      resolve: {
        extensions: ['*', '.ts', '.tsx', '.js', '.jsx'],
        alias: {
          ':scenes': path.resolve(__dirname, 'src/scenes/'),
          ':components': path.resolve(__dirname, 'src/components/'),
          ':images': path.resolve(__dirname, 'src/assets/images/'),
          ':types': path.resolve(__dirname, 'src/types'),
          ':constants': path.resolve(__dirname, 'src/constants'),
          ':api': path.resolve(__dirname, 'src/api'),
          ':utils': path.resolve(__dirname, 'src/utils'),
          ':actions': path.resolve(__dirname, 'src/redux/actions/'),
          ':sharedStyles': path.resolve(__dirname, 'src/styles/index.scss')
        }
      },
      plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
          template: path.resolve(__dirname, 'index.html')
        }),
        new webpack.DefinePlugin({
          ENV_BACKEND_ADDRESS: JSON.stringify(process.env.BACKEND_ADDRESS || '')
        }),
        new MiniCssExtractPlugin()
      ],
      optimization: {
        minimizer: [new UglifyJsPlugin(), new OptimizeCSSAssetsPlugin({})],
        splitChunks: {
          chunks: 'all'
        }
      }
    }
  }
}
