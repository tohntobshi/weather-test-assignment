# Тестовое задание

## Задача:​

Cделать небольшое независимое приложение на React, показывающее погоду в Москве на неделю. Нужно сверстать его и прикрутить к API https://www.metaweather.com/api/

## Demo

https://weather-test-assignment.firebaseapp.com

## Development

To work with this project run `npm install` (in the root of this project) after cloning it from its repository (node and npm should be installed).

In order to work this project needs `.env` file in its root directory with backend address provided like this

    BACKEND_ADDRESS=https://httpstat.us

For development run

    npm start

To build this run

    npm run build

