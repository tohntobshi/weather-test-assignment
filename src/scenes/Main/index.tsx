import React from 'react'
import { connect } from 'react-redux'
import { CSSTransition } from 'react-transition-group'
import * as Actions from ':actions'
import { RootState } from ':types'
import classnames from 'classnames'
import ForecastListElement from ':components/ForecastListElement'
const styles = require('./styles.scss')

const mapStateToProps = ({
  loading,
  forecast,
  error
}: RootState) => ({
  loading,
  forecast,
  error
})

const mapDispatchToProps = {
  requestForecast: Actions.requestForecast
}

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps

const Sample = ({
  loading,
  forecast,
  error,
  requestForecast
}: Props) => {
  return (
    <div className={classnames('transitional', styles.background)}>
      <div className={styles.container}>
        <CSSTransition
          classNames='route-transition'
          timeout={300}
          in={loading}
          unmountOnExit
        >
          <div className='spinnerBackground'>
            <div className='spinner' />
          </div>
        </CSSTransition>
        <div className={styles.header}>
          <button className='textBtn text16' onClick={requestForecast}>Refresh</button>
        </div>
        {
          error
            ? <div className={styles.errorContainer}>
              <p className='text12'>{error}</p>
            </div>
            : <div className={styles.list}>
              {
                forecast.map(el => <ForecastListElement key={el.id} data={el} />)
              }
            </div>
        }

      </div>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Sample)
