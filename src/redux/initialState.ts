import { RootState } from ':types'

export const INITIAL_STATE: RootState = {
  loading: false,
  forecast: [],
  error: ''
}
