import { createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'
import * as Actions from ':actions'
import { RootState } from ':types'
import { INITIAL_STATE } from '../initialState'

function setState(state: RootState, action: ReturnType<typeof Actions.setState>): RootState {
  return { ...state, ...action.state }
}

const HANDLERS = {
  [Actions.SET_STATE]: setState
}

export default createReducer<RootState, AnyAction>(INITIAL_STATE, HANDLERS)
