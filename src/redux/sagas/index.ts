import { takeLatest, put, call } from 'redux-saga/effects'
import * as Actions from ':actions'
import { getForecast } from ':api'

export function * requestForecast(action: ReturnType<typeof Actions.requestForecast>) {
  yield put(Actions.setState({ loading: true }))
  try {
    const forecast: UnpackedReturnedPromise<typeof getForecast> = yield call(getForecast)
    yield put(Actions.setState({ forecast, error: '' }))
  } catch (e) {
    yield put(Actions.setState({ forecast: [], error: e.message }))
  }
  yield put(Actions.setState({ loading: false }))
}

export default function * rootSaga() {
  yield takeLatest(Actions.REQUEST_FORECAST, requestForecast)
  // initial request
  yield put(Actions.requestForecast())
}
