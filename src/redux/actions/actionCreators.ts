import * as Actions from './actionTypes'
import { RootState } from ':types'

export const setState = (state: Partial<RootState>) => ({ type: Actions.SET_STATE, state })
export const requestForecast = () => ({ type: Actions.REQUEST_FORECAST })
