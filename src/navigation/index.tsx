import React from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import { createBrowserHistory } from 'history'
import Main from ':scenes/Main'

export const history = createBrowserHistory()

const routes: { path: string; component: React.ComponentType }[] = [
  { path: '/', component: Main }
]

export const Navigator = () => (
  <Router history={history}>
    <Route render={({ location }) => (
      <TransitionGroup>
        <CSSTransition
          key={location.key}
          classNames='route-transition'
          timeout={300}
        >
          <Switch location={location}>
            {
              routes.map((el) => <Route key={el.path} path={el.path} component={el.component} />)
            }
          </Switch>
        </CSSTransition>
      </TransitionGroup>
    )} />
  </Router>
)
