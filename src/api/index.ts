import { BACKEND_ADDRESS } from '../config'
import { ForecastEntry } from ':types'

export function getForecast(): Promise<ForecastEntry[]> {
  return fetch(`${BACKEND_ADDRESS}/api/location/2122265/`)
    .then((res) => {
      if (!res.ok) {
        throw new Error('Something went wrong')
      }
      return res.json()
    })
    .then(res => res['consolidated_weather'])
}
