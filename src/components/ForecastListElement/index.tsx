import React from 'react'
import { ForecastEntry } from ':types'
import classnames from 'classnames'
import { daysOfWeek, months } from ':constants'
const styles = require('./styles.scss')

interface Props {
  data: ForecastEntry;
}

const ForecastListElement = ({
  data
}: Props) => {
  const { dayOfWeek, dayOfMonth, month } = React.useMemo(() => {
    const date = new Date(data.applicable_date)
    const dayOfWeek = daysOfWeek[date.getDay()]
    const dayOfMonth = date.getDate()
    const month = months[date.getMonth()]
    return { dayOfWeek, dayOfMonth, month }
  }, [data.applicable_date])
  const maxTemp = Math.round(data.max_temp)
  const minTemp = Math.round(data.min_temp)
  return (
    <div className={classnames(
      styles.container,
      styles[data.weather_state_abbr]
    )}>
      <div className={styles.dayContainer}>
        <p className='text16'>{dayOfWeek}</p>
        <p className='text12'>{dayOfMonth} {month}</p>
      </div>
      <div className={styles.tempContainer}>
        <span className='text16'>{maxTemp} / {minTemp}</span>
        <div className={styles.icon} />
      </div>
    </div>
  )
}

export default ForecastListElement
