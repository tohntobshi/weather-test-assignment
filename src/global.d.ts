type UnpackedPromise<T> = T extends Promise<infer U> ? U : unknown
type UnpackedReturnedPromise<T extends (...args: any[]) => Promise<any>> = UnpackedPromise<ReturnType<T>>
